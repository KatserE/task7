class Car {
    weak var driver: Man?

    deinit{
        print("Car object is removed from memory" )
    }
}

class Man {
    var myCar: Car?

    deinit{
        print("Man object is removed from memory")
    }
}

var car: Car? = Car()
var man: Man? = Man()

car?.driver = man
man?.myCar = car

car = nil
man = nil
