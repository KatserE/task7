Capture list is a special swift syntax for closures that helps us to set different
types to references. That list should be before parameters of closures and in
keyword. For example:
```swift
do { // Create a scope
    let a = 5, b = 10
    let _ = {[a, b] in
        print(a, b)
    }
}
```

Closures always capture all objects by strong reference even value types. So if
value type is changed, that value in closure is also changed. If value objects
shouldn't be changed, we need put them into capture list as in example above.

Now let's talk about main reason for using capture list -- retain cycles. The
problem of this situation is memory leaks. In swift we use ARC (automatic
reference counting) to remove reference types from memory. The object is deleted
when number of strong reference to it is equal to 0. So when we have retain cycle,
the number of references to objects never gets 0, so ARC can't remove them from
memory. The example of simple retain cycle is provided below:
```swift
class DriverWrong { var car: CarWrong? }
class CarWrong { var driver: DriverWrong? }

do {
    let driver = DriverWrong()
    let car = CarWrong()

    // Create retain cycle
    driver.car = car // Driver holds car        | => each number of references
    car.driver = driver // Car holds driver     | is equal to 2
}
```

So the number of strong reference to `driver` = 2 (1 from `let driver` and 1 from
`car` object). Number of strong reference to `car` also = 2 (1 from `let car` and
1 from `driver` object). After deleting `let car` and `let driver` each object
will have 1 strong reference, so they won't be deleted.

The solution is make one reference as weak or unowned (the difference in the end).
We can make both reference weak or unowened but it's overkill. Weak of unowned
reference don't increase the counter of strong reference. After fix we get the
code below:
```swift
class DriverRight { var car: CarRight? }
class CarRight { weak var driver: DriverRight? }

do {
    let driver = DriverRight()
    let car: CarRight = CarRight()

    driver.car = car // Driver holds car. Number of strong reference = 1
    car.driver = driver // Car only "looks" to driver. Number of strong reference = 2
}
```

So the number of strong reference to `driver` = 1 (only from `let driver`). Number
of strong reference to `car` = 2. After deleting `let car` strong reference to
`car` object equals 1. After deleting `driver` object strong reference to `car`
object equals 0 so both object will be deleted.

A reader can think that we don't have classes, we just have closures so we can't
get retain cycle. We can and capture list hepls us to remove it. Let's start from
retain cycle in closure. For example:
```swift
class DriverClosure {
    var carInfoPrinter: (() -> ())?
    func printInfo() { print("Very careful driver!") }
}
class CarClosure {
    var driverInfoPrinter: (() -> ())?
    func printInfo() { print("Very beautiful car!") }
}

do {
    let driver = DriverClosure()
    let car = CarClosure()

    driver.carInfoPrinter = car.printInfo
    car.driverInfoPrinter = driver.printInfo
}
```

In example above we have `carInfoPrinter` that holds car object and
`driverInfoPrinter` that holds driver object => retain cycle. To fix that error
we use capture list and set one reference as weak:
```swift
do {
    let driver = DriverClosure()
    let car = CarClosure()

    driver.carInfoPrinter = car.printInfo
    car.driverInfoPrinter = { [weak driver] in driver?.printInfo() }
}
```

For creating a retain cycle we also can use only one class. For example:
```swift
do {
    let driver = DriverClosure()
    driver.carInfoPrinter = { driver.printInfo() } // By mistake
}
```

In example above we made wrong closures by mistake and got a retain cycle!
The `carInfoPrinter` holds a strong reference by itself! To fix that error we
can make driver unowned or weak:
```swift
do {
    let driver = DriverClosure()
    driver.carInfoPrinter = { [unowned driver] in driver.printInfo() } // By mistake
}
```

Likewise we can create a retain cycle inside one class with lazy closure. For
example:
```swift
class ClassWithLazyRetainCycle {
    let a = 5
    lazy var closure = {
        print(self.a)
    }
}

do {
    let classObj = ClassWithLazyRetainCycle()
    classObj.closure()
}
```

In example above we create a lazy closure that prints one field of class. If we
don't use it, there aren't any memory leaks. The closure don't init so we don't
have strong reference from closure to self. But if we use it, closure is initialized
and we get a memory leak because of closure handle strong reference to self and
self handle strong reference to closure. To fix it we can use weak or unowned
reference:
```swift
class ClassWithoutRetainCycle {
    let a = 5
    lazy var closure = { [unowned self] in
        print(self.a)
    }
}

do {
    let classObj = ClassWithoutRetainCycle()
    classObj.closure()
}
```

Also sometimes we can create not retain cycle but a retain line. Closure can holds
an object, that holds an object, that holds an object and so on. For example:
```swift
var closureWithRetainLine: (() -> ())?
do {
    let driver = DriverRight()
    let car = CarRight()

    driver.car = car // Usually there isn't any problem
    car.driver = driver

    // But today...
    closureWithRetainLine = { print(type(of: driver)) }
}
```

Memory will be freed only when closure will be deleted. In that moment strong
reference from closure to one object will be removed and memory will be freed.
But sometimes it can be a very long time. So we can use a capture list with
weak reference to free resources after a block end:
```swift
var closureWithoutRetainLine: (() -> ())?
do {
    let driver = DriverRight()
    let car = CarRight()

    driver.car = car
    car.driver = driver

    closureWithoutRetainLine = { [weak driver] in print(type(of: driver)) }
}
```

If you are afraid of retain cycle but you don't wanna use weak reference and
you can guarantee that object won't be deleted during closure using, you can
use unowned reference.
```swift
do {
    var closureWithUnowned: (() -> ())?
    let driver = DriverRight()
    let car = CarRight()

    driver.car = car
    car.driver = driver

    closureWithUnowned = { [unowned driver] in print(type(of: driver)) }
    closureWithUnowned!()
}
```

Now you can be confused by difference with weak and unowned references. If we
are talking about class properties, a weak reference must be an optional type.
The restrict exists because weak reference become nil when the object under it
is deleted. By the way, because of switch weak reference must be var property.

An unowned reference presumes that it will never become nil during its lifetime.
An unowned reference must be set during initialization - this means that the
reference will be defined as a non-optional type that can be used safely without
checks. If somehow the object being referred to is deallocated, then the app will
crash when the unowned reference is used.

If we are talking about closures, the types don't help us because closure can
create optional or non-optional type for us. So the main point in this situation
is the possibility of deallocating object under the reference during using the
closure. If it might be, we must use weak reference, if object will live the same
time as a closure, we can use unowned reference to avoid of force unwrappings
reference.
