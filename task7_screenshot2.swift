class Man {
    var pasport: Passport? // You can't put weak or unowned here!

    deinit {
        print("Man object is removed from memory")
        pasport = nil
    }
}

class Passport {
    unowned let man: Man // Add unowned here

    init (man: Man) {
        self.man = man
    }

    deinit {
        print("Passport object is removed from memory")
    }
}

var man: Man? = Man()
var passport: Passport? = Passport(man: man!)
man?.pasport = passport
passport = nil // Passport object is holded by `man` object
man = nil // Both objects are removed from memory
