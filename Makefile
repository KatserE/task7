OUTPUT			:= "./output/"

VEHICLES_SRC	:= "task7_vehicles.swift"
VEHICLES_NAME	:= "task7_vehicles"

SCREEN1_SRC		:= "task7_screenshot1.swift"
SCREEN1_NAME	:= "task7_screenshot1"

SCREEN2_SRC		:= "task7_screenshot2.swift"
SCREEN2_NAME	:= "task7_screenshot2"

all: output_dir $(VEHICLES_NAME) $(SCREEN1_NAME) $(SCREEN2_NAME)
	@echo Compilation is successful!

output_dir:
	@test -d $(OUTPUT) || mkdir $(OUTPUT)

$(VEHICLES_NAME):
	@swiftc $(VEHICLES_SRC) -o $(OUTPUT)/$(VEHICLES_NAME)

$(SCREEN1_NAME):
	@swiftc $(SCREEN1_SRC) -o $(OUTPUT)/$(SCREEN1_NAME)

$(SCREEN2_NAME):
	@swiftc $(SCREEN2_SRC) -o $(OUTPUT)/$(SCREEN2_NAME)

clean:
	@test -d $(OUTPUT) && rm -rf $(OUTPUT)
