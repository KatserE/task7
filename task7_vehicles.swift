// MARK: - Describe structs for a passenger car and a truck
// MARK: Struct that describes cargo
struct Cargo : CustomStringConvertible, Hashable {
    let volume: UInt
    var description: String {
        "Cargo with volume \(volume)"
    }
}

// MARK: Enumeration with all possible actions with vehicle
enum VehicleAction {
    case StartEngine, StopEngine
    case OpenWindows, CloseWindows
    case LoadCargo(Cargo), UnloadCargo(Cargo)
}

enum VehicleError : Error {
    case IsBroken
    case ThereIsntEnoughSpaceForCargo, ThereIsntCargo
}

// MARK: Protocol that describes requirements for an object with a full name
protocol FullNamed {
    var fullName: String { get }
}

// MARK: Protocol that describes requirements for a vehicle
protocol Vehicle : FullNamed {
    var brand: String { get }
    var model: String { get }
    var yearOfManufacture: UInt16 { get }
    var volume: UInt { get }
    var engineIsStarted: Bool { get }
    var windowsIsOpen: Bool { get }
    var filled: [Cargo] { get }

    mutating func make(action: VehicleAction) throws
}

// MARK: Default defenition for fullName property of a vehicle
extension Vehicle {
    var fullName: String { brand + " " + model }
}

extension Vehicle {
    func isEnoughtSpace(for newCargo: Cargo) -> Bool {
        volume - filled.reduce(0) { $0 + $1.volume } >= newCargo.volume
    }
}

// MARK: The struct that describes a passenger car
struct PassengerCar : Vehicle, Hashable {
    let brand: String
    let model: String
    let yearOfManufacture: UInt16
    let volume: UInt // Don't consider trailers
    var engineIsStarted: Bool = false
    var windowsIsOpen: Bool = false
    var filled: [Cargo] = []

    mutating func make(action: VehicleAction) throws {
        switch action {
            case .StartEngine: engineIsStarted = true
            case .StopEngine: engineIsStarted = false
            case .OpenWindows: windowsIsOpen = true
            case .CloseWindows: windowsIsOpen = false
            case let .LoadCargo(cargo): try loadCargo(cargo)
            case let .UnloadCargo(cargo): try unloadCargo(cargo)
        }
    }

    private mutating func loadCargo(_ cargo: Cargo) throws {
        guard isEnoughtSpace(for: cargo) else {
            throw VehicleError.ThereIsntEnoughSpaceForCargo
        }

        filled.append(cargo)
    }

    private mutating func unloadCargo(_ cargo: Cargo) throws {
        let index = filled.firstIndex { $0.volume == cargo.volume }
        guard let index else {
            throw VehicleError.ThereIsntCargo
        }

        filled.remove(at: index)
    }
}

// MARK: The struct that describes a truck
struct Truck : Vehicle, Hashable {
    let brand: String
    let model: String
    let yearOfManufacture: UInt16
    let volume: UInt // Don't consider trailers
    var engineIsStarted: Bool = false
    var windowsIsOpen: Bool = false
    var filled: [Cargo] = []
    var isBroken = false

    mutating func make(action: VehicleAction) throws {
        switch action {
            case .StartEngine:
                engineIsStarted = true
            case .StopEngine:
                engineIsStarted = false
            case .OpenWindows:
                windowsIsOpen = true
            case .CloseWindows:
                windowsIsOpen = false
            case let .LoadCargo(cargo):
                guard !isBroken else {
                    throw VehicleError.IsBroken
                }

                if !isEnoughtSpace(for: cargo) {
                    isBroken = true
                }
                filled.append(cargo)
            case let .UnloadCargo(cargo):
                let index = filled.firstIndex { $0.volume == cargo.volume }
                guard let index else {
                    throw VehicleError.ThereIsntCargo
                }

                filled.remove(at: index)
        }
    }
}

var ferrari = PassengerCar(
    brand: "Ferrari", model: "F8 Tributo", yearOfManufacture: 2020,
    volume: 200
)
var porsche = PassengerCar(
    brand: "Porsche", model: "911 Turbo S", yearOfManufacture: 2020,
    volume: 128
)
var mercedes = Truck(
    brand: "Mercedes-Benz", model: "Atego", yearOfManufacture: 2016,
    volume: 42 * 1000
)

// MARK: Tests
print("Ferrari:")
try! ferrari.make(action: .StartEngine)
try! ferrari.make(action: .OpenWindows)
print(ferrari.engineIsStarted ? "Engine is started" : "Engine is stopped")
print(ferrari.windowsIsOpen ? "Windows is opened" : "Windows is closed")
print("Load small cargo:")
try! ferrari.make(action: .LoadCargo(Cargo(volume: 5)))
print("Cargo -- \(ferrari.filled)")
try! ferrari.make(action: .StopEngine)
print(ferrari.engineIsStarted ? "Engine is started" : "Engine is stopped")
print("Unload cargo")
try! ferrari.make(action: .UnloadCargo(Cargo(volume: 5)))
print("Cargo -- \(ferrari.filled)")

print("\nMercedes:")
print("Try to load giant cargo:")
try! mercedes.make(action: .LoadCargo(Cargo(volume: 100 * 100_000)))
print(mercedes.isBroken ? "Mercedes is broken!" : "Mercedes isn't broken!")
do {
    print("Try to load another little cargo:")
    try mercedes.make(action: .LoadCargo(Cargo(volume: 5)))
} catch VehicleError.IsBroken {
    print("Can't put more!")
}

print("\nPorsche:")
print("Put cargo")
try porsche.make(action: .LoadCargo(Cargo(volume: 10)))
print(porsche.filled)
do {
    print("Try to get cargo with volume 5:")
    try porsche.make(action: .UnloadCargo(Cargo(volume: 5)))
} catch VehicleError.ThereIsntCargo {
    print("There isn't this cargo!")
}
do {
    print("Try to load large cargo:")
    try porsche.make(action: .LoadCargo(Cargo(volume: 200)))
} catch VehicleError.ThereIsntEnoughSpaceForCargo {
    print("There isn't enought space for cargo!")
}

// We can't use protocol as a key in dictionaries sooo
var passengerCars = [
    ferrari : ferrari.fullName,
    porsche : porsche.fullName
]
var trucks = [
    mercedes : mercedes.fullName
]

print("\nIterate through dictionaries:")
print("Passanger cars:")
passengerCars.forEach { print("\($0.key.brand) -- \($0.value)") }
print("\nTrucks:")
trucks.forEach { print("\($0.key.brand) -- \($0.value)") }
